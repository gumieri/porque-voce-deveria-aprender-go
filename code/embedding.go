package main

// START OMIT
type Reader interface {
	Read(p []byte) (n int, err error)
}

type Writer interface {
	Write(p []byte) (n int, err error)
}

type ReadWriter interface {
	Reader // HL
	Writer // HL
}

// END OMIT

func main() {
}
