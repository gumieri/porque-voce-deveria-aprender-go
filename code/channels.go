package main

func main() {
	// START OMIT
	c := make(chan int) // Allocate a channel. // HL

	// Start the sort in a goroutine; when it completes, signal on the channel.
	go func() {
		list.Sort()
		c <- 1 // Send a signal; value does not matter. // HL
	}()

	<-c // Wait for sort to finish; discard sent value. // HL
	// END OMIT
}
