package main

import "fmt"

// START OMIT
func A(b func()) {
	b()
}

func B() {
	fmt.Println("Hello world!")
}

func main() {
	A(B) // HL
}

// END OMIT
