
# Apresentação - Linguagem de programação Go

## Como abrir os slides

    $ git clone http://srv-gitlab.tecnospeed.local/rafael.gumieri/apresentacao-linguagem-de-programacao-go.git
    $ cd porque-voce-deveria-aprender-go
    $ go get golang.org/x/tools/cmd/present
    $ present

E abra o navegador em http://127.0.0.1:3999/

## Como preparar os containers do exemplo

Você precisará de Docker 1.9+ e Docker Compose 1.7+

    $ cd demo
    $ docker-compose up -d db

Para popular o banco com dados de exemplo:

    $ docker-compose exec db psql -U postgres
    postgres> \i /tmp/dump.sql
    app> \q

## Como executar o exemplo

    $ cp sample/*.go .
    $ docker-compose up app

E acesse http://127.0.0.1
