package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

// conclui uma requisição http
func done(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Fatal(err)
	}
}

// conclui uma requisição http com um erro
func fail(w http.ResponseWriter, err error) {
	failWith(w, err, http.StatusInternalServerError)
}

// concluir uma requisição http com erro e um código de status customizado
func failWith(w http.ResponseWriter, err error, status int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	if e := json.NewEncoder(w).Encode(map[string]string{"error": err.Error()}); e != nil {
		log.Fatal(e)
	}
}

// converte o ID passado por uma URL
func parseID(w http.ResponseWriter, v map[string]string, k string) int {
	id, err := strconv.Atoi(v[k])
	if err != nil {
		failWith(w, err, http.StatusBadRequest)
		return 0
	}
	return id
}

// consulta o banco de dados, chamando uma função passando os resultados da consulta
func queryAll(f func(rows *sql.Rows) error, query string, args ...interface{}) (err error) {
	rows, err := db.Query(query, args...)
	if err != nil {
		return
	}
	defer rows.Close()

	if err = rows.Err(); err != nil {
		return
	}

	for rows.Next() {
		if err = f(rows); err != nil {
			break
		}
	}
	return
}

// abre uma transação, chama uma função passando essa transação. Commita se ocorrer tudo normalmente e cancela a transação de algum erro for emitido
func inTx(f func(tx *sql.Tx) error) error {
	tx, err := db.Begin()
	if err == nil {
		if err = f(tx); err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}
	return err
}
