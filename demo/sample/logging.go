package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

const logFormat = "%s - - [%s] \"%s %d %d\" %f\n"

type rec struct {
	http.ResponseWriter

	ip        string
	time      time.Time
	method    string
	uri       string
	proto     string
	status    int
	respBytes int64
	elapsed   time.Duration
}

func (r *rec) Log(out io.Writer) {
	tf := r.time.Format("02/Jan/2006 03:04:05")
	reqLine := fmt.Sprintf("%s %s %s", r.method, r.uri, r.proto)
	fmt.Fprintf(out, logFormat, r.ip, tf, reqLine, r.status, r.respBytes, r.elapsed.Seconds())
}

func (r *rec) Write(p []byte) (int, error) {
	written, err := r.ResponseWriter.Write(p)
	r.respBytes += int64(written)
	return written, err
}

func (r *rec) WriteHeader(status int) {
	r.status = status
	r.ResponseWriter.WriteHeader(status)
}

type decorator func(http.HandlerFunc) http.HandlerFunc

func logging(out io.Writer, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		clientIP := r.RemoteAddr
		if colon := strings.LastIndex(clientIP, ":"); colon != -1 {
			clientIP = clientIP[:colon]
		}

		rwr := &rec{
			ResponseWriter: w,
			ip:             clientIP,
			time:           time.Time{},
			method:         r.Method,
			uri:            r.RequestURI,
			proto:          r.Proto,
			status:         http.StatusOK,
			elapsed:        time.Duration(0),
		}

		start := time.Now()
		next(rwr, r)
		finish := time.Now()

		rwr.time = finish.UTC()
		rwr.elapsed = finish.Sub(start)

		rwr.Log(out)
	}
}
