package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	defer db.Close()

	router := mux.NewRouter()
	router.Methods("GET").Path("/").HandlerFunc(logging(os.Stdout, questions))
	router.Methods("POST").Path("/").HandlerFunc(logging(os.Stdout, newQuestion))
	router.Methods("GET").Path("/{questionID}").HandlerFunc(logging(os.Stdout, results))
	router.Methods("POST").Path("/{questionID}").HandlerFunc(logging(os.Stdout, newChoice))
	router.Methods("PUT").Path("/{questionID}/{choiceID}").HandlerFunc(logging(os.Stdout, vote))

	http.ListenAndServe(":"+os.Getenv("PORT"), router)
}

func questions(w http.ResponseWriter, r *http.Request) {
	var data []question
	queryErr := queryAll(func(rows *sql.Rows) (err error) {
		var q question
		if err = rows.Scan(&q.ID, &q.QuestionText, &q.PubDate); err != nil {
			return
		}
		data = append(data, q)
		return
	}, SQLQueryQuestions)

	if queryErr != nil {
		fail(w, queryErr)
	} else {
		done(w, data)
	}
}

func newQuestion(w http.ResponseWriter, r *http.Request) {
	var q question

	if err := json.NewDecoder(r.Body).Decode(&q); err != nil {
		fail(w, err)
		return
	}

	if q.QuestionText == "" {
		failWith(w, errors.New("\"question_text\" deve estar preenchido"), http.StatusBadRequest)
		return
	}
	q.PubDate = time.Now()

	txErr := inTx(func(tx *sql.Tx) (err error) {
		err = tx.QueryRow(SQLNewQuestion, q.QuestionText, q.PubDate).Scan(&q.ID)
		return
	})
	if txErr != nil {
		fail(w, txErr)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
}

func results(w http.ResponseWriter, r *http.Request) {
	v := mux.Vars(r)
	questionID := parseID(w, v, "questionID")
	if questionID < 1 {
		return
	}

	var data []choice

	queryErr := queryAll(func(rows *sql.Rows) (err error) {
		var c choice
		c.QuestionID = questionID
		if err = rows.Scan(&c.ID, &c.ChoiceText, &c.Votes); err != nil {
			return
		}
		data = append(data, c)
		return
	}, SQLQueryResults, questionID)

	if queryErr != nil {
		fail(w, queryErr)
	} else {
		done(w, data)
	}
}

func newChoice(w http.ResponseWriter, r *http.Request) {
	v := mux.Vars(r)
	questionID := parseID(w, v, "questionID")
	if questionID < 1 {
		return
	}

	var c choice
	if err := json.NewDecoder(r.Body).Decode(&c); err != nil {
		fail(w, err)
		return
	}

	if c.ChoiceText == "" {
		failWith(w, errors.New("\"choice_text\" deve ser preenchido"), http.StatusBadRequest)
		return
	}
	c.QuestionID = questionID
	c.Votes = 0

	txErr := inTx(func(tx *sql.Tx) (err error) {
		err = tx.QueryRow(SQLNewChoice, questionID, c.ChoiceText).Scan(&c.ID)
		return
	})
	if txErr != nil {
		fail(w, txErr)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
}

func vote(w http.ResponseWriter, r *http.Request) {
	v := mux.Vars(r)
	questionID := parseID(w, v, "questionID")
	choiceID := parseID(w, v, "choiceID")
	if questionID < 1 || choiceID < 1 {
		return
	}

	var c int64
	txErr := inTx(func(tx *sql.Tx) (err error) {
		res, err := tx.Exec(SQLVote, questionID, choiceID)
		if err != nil {
			return
		}
		c, err = res.RowsAffected()
		return
	})
	if txErr != nil {
		fail(w, txErr)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if c > 0 {
		w.WriteHeader(http.StatusNoContent)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}
