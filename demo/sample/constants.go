package main

const (
	SQLQueryQuestions = "select id, question_text, pub_date from questions"
	SQLQueryResults   = "select id, choice_text, votes from choices where question_id = $1 order by id"
	SQLNewQuestion    = "insert into questions (question_text, pub_date) values ($1, $2) returning id"
	SQLNewChoice      = "insert into choices (question_id, choice_text) values ($1, $2) returning id"
	SQLVote           = "update choices set votes = votes + 1 where question_id = $1 and id = $2"
)
