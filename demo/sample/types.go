package main

import "time"

type question struct {
	ID           int       `json:"id"`
	QuestionText string    `json:"question_text"`
	PubDate      time.Time `json:"pub_date"`
}

type choice struct {
	ID         int    `json:"id"`
	QuestionID int    `json:"question_id"`
	ChoiceText string `json:"choice_text"`
	Votes      int    `json:"votes"`
}

var sampleQuestions = []question{
	{
		ID:           1,
		QuestionText: "Você conhece Go?",
		PubDate:      time.Date(2016, time.June, 8, 21, 3, 8, 0, time.UTC),
	},
	{
		ID:           2,
		QuestionText: "Porque Go é tão incrível?",
		PubDate:      time.Date(2016, time.June, 8, 21, 3, 47, 0, time.UTC),
	},
}
