--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

-- Started on 2016-06-08 21:10:03 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE app;
--
-- TOC entry 2124 (class 1262 OID 16384)
-- Name: app; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE app WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE app OWNER TO postgres;

\connect app

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2125 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 1 (class 3079 OID 12361)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2127 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 16399)
-- Name: choices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE choices (
    id integer NOT NULL,
    question_id integer NOT NULL,
    choice_text text NOT NULL,
    votes integer DEFAULT 0 NOT NULL
);


ALTER TABLE choices OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16397)
-- Name: choices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE choices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE choices_id_seq OWNER TO postgres;

--
-- TOC entry 2128 (class 0 OID 0)
-- Dependencies: 183
-- Name: choices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE choices_id_seq OWNED BY choices.id;


--
-- TOC entry 182 (class 1259 OID 16387)
-- Name: questions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE questions (
    id integer NOT NULL,
    question_text text NOT NULL,
    pub_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE questions OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16385)
-- Name: questions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questions_id_seq OWNER TO postgres;

--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 181
-- Name: questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questions_id_seq OWNED BY questions.id;


--
-- TOC entry 1995 (class 2604 OID 16402)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choices ALTER COLUMN id SET DEFAULT nextval('choices_id_seq'::regclass);


--
-- TOC entry 1993 (class 2604 OID 16390)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions ALTER COLUMN id SET DEFAULT nextval('questions_id_seq'::regclass);


--
-- TOC entry 2119 (class 0 OID 16399)
-- Dependencies: 184
-- Data for Name: choices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO choices (id, question_id, choice_text, votes) VALUES (1, 1, 'Ainda não', 0);
INSERT INTO choices (id, question_id, choice_text, votes) VALUES (2, 1, 'Quero muito!', 0);
INSERT INTO choices (id, question_id, choice_text, votes) VALUES (3, 2, 'Sintaxe simples', 0);
INSERT INTO choices (id, question_id, choice_text, votes) VALUES (4, 2, 'Fácil de aprender', 0);
INSERT INTO choices (id, question_id, choice_text, votes) VALUES (5, 2, 'Muitos recursos', 0);
INSERT INTO choices (id, question_id, choice_text, votes) VALUES (6, 2, 'Comunidade muito ativa', 0);


--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 183
-- Name: choices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('choices_id_seq', 6, true);


--
-- TOC entry 2117 (class 0 OID 16387)
-- Dependencies: 182
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO questions (id, question_text, pub_date) VALUES (1, 'Você conhece Go?', '2016-06-08 21:03:08.601676');
INSERT INTO questions (id, question_text, pub_date) VALUES (2, 'Porque Go é tão incrível?', '2016-06-08 21:03:47.64967');


--
-- TOC entry 2131 (class 0 OID 0)
-- Dependencies: 181
-- Name: questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('questions_id_seq', 2, true);


--
-- TOC entry 2000 (class 2606 OID 16408)
-- Name: choices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choices
    ADD CONSTRAINT choices_pkey PRIMARY KEY (id);


--
-- TOC entry 1998 (class 2606 OID 16396)
-- Name: questions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- TOC entry 2001 (class 2606 OID 16409)
-- Name: choices_questions_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY choices
    ADD CONSTRAINT choices_questions_fk FOREIGN KEY (question_id) REFERENCES questions(id);


--
-- TOC entry 2126 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-06-08 21:10:03 UTC

--
-- PostgreSQL database dump complete
--
